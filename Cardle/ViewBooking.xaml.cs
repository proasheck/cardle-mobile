﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardle.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Xamarin.Forms.Maps;
using Xamarin.Essentials;
using Rg.Plugins.Popup.Services;

namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewBooking : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public ViewBooking()
        {
            InitializeComponent();
            UpdateMap();
            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(1.3521, 103.8198), Distance.FromKilometers(15)));
            

        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            Booking booking = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.BookingID);
            Time.Text = booking.TripTime.ToString();
            Date.Text = booking.TripDate.ToShortDateString().ToString();
            var user = await firebaseHelper.GetUserData(booking.RiderID);
            Name.Text = user.Username;

        }
        private async void UpdateMap()
        {
            Booking booking = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.BookingID);

            try
            {
                MyMap.Pins.Clear();
                var Fromaddress = booking.StartOfDest;
                var locations = await Geocoding.GetLocationsAsync(Fromaddress);

                var location = locations?.FirstOrDefault();
                if (location != null)
                {
                    Pin from = new Pin
                    {
                        Label = "Location",
                        Address = "Pickup Location",
                        Type = PinType.SearchResult,
                        Position = new Position(location.Latitude, location.Longitude)
                    };
                    MyMap.Pins.Add(from);

                }
                var Toaddress = booking.EndOfDest;
                var destinations = await Geocoding.GetLocationsAsync(Toaddress);
                var destination = destinations?.FirstOrDefault();
                if (destination != null)
                {
                    Pin to = new Pin
                    {
                        Label = "Destination",
                        Address = "Drop off Location",
                        Type = PinType.SearchResult,
                        Position = new Position(destination.Latitude, destination.Longitude)
                    };

                    MyMap.Pins.Add(to);

                }
                //MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position((location.Latitude + destination.Latitude) / 2, (location.Longitude + destination.Longitude) / 2), Distance.FromKilometers(5)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }
        private async void AcceptBookingButtonClicked(object sender, EventArgs e)
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

            // Accepting Booking Code
            Booking booking = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.BookingID);
            if (booking.Status == "Accepted")
            {
                await DisplayAlert("Error", "Booking has already been accepted", "OK");
            }
            else if (user.ID == booking.RiderID)
            {
                await DisplayAlert("Error", "You cannot accept your own booking", "OK");

            }
            else
            {
                await firebaseHelper.UpdateBookings(booking.Name, booking.BookingID, booking.StartOfDest, booking.EndOfDest, booking.TripDate, booking.TripTime, user.ID, booking.RiderID, "Accepted");
                await DisplayAlert("Success", "Booking has been accepted", "OK");
                await Navigation.PushAsync(new Bookings { });
            }
        }
        private async void CancelBookingButtonClicked(object sender, EventArgs e)
        {
            Booking booking = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.BookingID);
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            if (user.ID == booking.RiderID)
            {
                await firebaseHelper.UpdateBookings(booking.Name, booking.BookingID, booking.StartOfDest, booking.EndOfDest, booking.TripDate, booking.TripTime, user.ID, booking.RiderID, "Cancelled");
                await DisplayAlert("Success", "Booking has been cancelled", "OK");
                await Navigation.PushAsync(new Bookings { });
            }
            else
            {
                await DisplayAlert("Error", "You can't Cancel this booking", "OK");

            }
        }
        private async void CompletedTripButtonClicked(object sender, EventArgs e)
        {
            Booking booking = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.BookingID);
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            var allTrips = await firebaseHelper.GetAllTrip();
            int x = 0;
            foreach (var i in allTrips)
            {
                if (Id != null)
                {
                    x += 2;
                }
            }
            int TripID = x;
            if (user.ID == booking.DriverID)
            {
                await firebaseHelper.UpdateBookings(booking.Name, booking.BookingID, booking.StartOfDest, booking.EndOfDest, booking.TripDate, booking.TripTime, user.ID, booking.RiderID, "Completed");
                await firebaseHelper.AddTrip(TripID, booking.Name, booking.BookingID, booking.DriverID, booking.RiderID);
                var data = await firebaseHelper.GetUserData(booking.RiderID);
                int id = data.ID;
                string username = data.Username;
                string pass = data.Password;
                string email = data.EmailAddress;
                string role = data.Role;
                int reward = data.Rewards;
                reward += 100;
                await firebaseHelper.UpdateUserReward(booking.RiderID, username, pass, email, role, reward);
                await DisplayAlert("Success", "Trip Complete", "OK");
                await PopupNavigation.Instance.PushAsync(new Payment());
            }

        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ViewRiderInterest{ });

        }
    }
}