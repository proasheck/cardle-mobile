﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cardle.Models
{
    public class Interest
    {
        public int UserID { get; set; }
        public bool Ethics { get; set; }
        public bool Geography { get; set; }
        public bool Gaming { get; set; }
        public bool History { get; set; }
        public bool Literature { get; set; }
        public bool Philosophy { get; set; }
        public bool Psychology { get; set; }
        public bool Politics { get; set; }
        public bool Sports { get; set; }
        public bool Science { get; set; }
        public bool Technology { get; set; }
        public bool OptOut { get; set; }
    }
}
