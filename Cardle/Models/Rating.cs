﻿using System;
using System.Collections.Generic;
using System.Text;
using Cardle.Models;

namespace Cardle.Models
{
	class Rating
	{
		public int RatingID { get; set; }
		public Trip TripID { get; set; }
		public int RatingGiven { get; set; }
	}
}
