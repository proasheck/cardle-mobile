﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cardle.Models
{
	public class Trip
	{
		public int TripID { get; set; }
		public string Name { get; set; }
		public int DriverID { get; set; }
		public int RiderID { get; set; }

        public string PassengerRating { get; set; }
         public string DriverRating { get; set; }

        public int BookingID { get; set; }

		public bool IsVisible { get; set; }
	}
}
