﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Cardle.Models
{
    public class User
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Username { get; set; }
        public string EmailAddress {get;set;}
        public string Password { get; set; }
        public string Role { get; set; }

        public string Avail { get; set; }

        public int Rewards { get; set; }

        public bool Subscription { get; set; }

        public string QRCode { get; set; }
        public bool Verified { get; set; }
    }
}
