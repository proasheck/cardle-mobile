﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cardle.Models
{
    public class Message
    {
        public int Sender { get; set; }
        public int Receiver { get; set; } 
        public string SenderName { get; set; }
        public string Msg { get; set; }
    }
}
