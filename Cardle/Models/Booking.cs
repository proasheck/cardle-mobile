﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cardle.Models
{
    public class Booking
    {
        public string Name { get; set; }
        public int BookingID { get; set; }
        public string StartOfDest { get; set; }
        public string EndOfDest { get; set; }
        public DateTime TripDate { get; set; }
        public TimeSpan TripTime { get; set; }
        public int DriverID { get; set; }
        public int RiderID { get; set; }
        public string Status { get; set; }
        public double distance { get; set; }

    }
}
