﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using Cardle.Models;
using Xamarin.Essentials;

namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public Home()
        {
            InitializeComponent();
            BookingDate.MinimumDate = DateTime.Now;
            BookingTime.Time = DateTime.Now.TimeOfDay;
            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(1.3521, 103.8198), Distance.FromKilometers(15)));
            Application.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, "#c6c0e4");
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            if (user.Subscription == true)
            {
                this.BindingContext = new
                {
                    Subscription = false,
                    payment = "Select Payment Type",
                    points = "Current Points: " + Convert.ToString(user.Rewards),
                    show = user.Subscription
                };
            }
            else
            {
                this.BindingContext = new
                {
                    Subscription = true,
                    payment = "Select Payment Type",
                    points = "Current Points: " + Convert.ToString(user.Rewards),
                    show = user.Subscription
                };
            }
        }
        //AddBooking(int bookingid, string startofdest, string endofdest, DateTime tripdate, TimeSpan triptime, int driverid, int riderid, string status)
        async void BookingButtonClicked(object sender, EventArgs e)
        {
            var allBookings = await firebaseHelper.GetAllBooking();
            int x = 0;
            foreach (var i in allBookings)
            {
                if (Id != null)
                {
                    x += 2;
                }
            }
            int BookingID = x;
            string Name = txtFromLocation.Text +" to " + txtToLocation.Text;
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            await firebaseHelper.AddBooking(Name,BookingID, txtFromLocation.Text, txtToLocation.Text, BookingDate.Date, BookingTime.Time,0, user.ID,"pending");
            await DisplayAlert("Success", "Booking Successfuly Added", "OK");

        }


        private async void UpdateMap()
        {
            try
            {
                MyMap.Pins.Clear();
                var Fromaddress = txtFromLocation.Text;
                var locations = await Geocoding.GetLocationsAsync(Fromaddress);

                var location = locations?.FirstOrDefault();
                if (location != null)
                {
                    Pin from = new Pin
                    {
                        Label = "Location",
                        Address = "Pickup Location",
                        Type = PinType.SearchResult,
                        Position = new Position(location.Latitude, location.Longitude)
                    };
                    MyMap.Pins.Add(from);
                }
                var Toaddress = txtToLocation.Text;
                var destinations = await Geocoding.GetLocationsAsync(Toaddress);
                var destination = destinations?.FirstOrDefault();
                if (destination != null)
                {
                    Pin to = new Pin
                    {
                        Label = "Destination",
                        Address = "Drop off Location",
                        Type = PinType.SearchResult,
                        Position = new Position(destination.Latitude, destination.Longitude)
                    };

                    MyMap.Pins.Add(to);                   
                }
                Xamarin.Essentials.Location sourceCoordinates = new Xamarin.Essentials.Location(location.Latitude, location.Longitude);
                Xamarin.Essentials.Location destinationCoordinates = new Xamarin.Essentials.Location(destination.Latitude, destination.Longitude);
                double distance = Xamarin.Essentials.Location.CalculateDistance(location, destination, DistanceUnits.Kilometers);
                var bookingdetails = new Booking
                {
                    StartOfDest = txtFromLocation.Text,
                    EndOfDest = txtToLocation.Text,
                    distance = distance
                };
                string bookingdetail = JsonConvert.SerializeObject(bookingdetails);
                Cardle.Data.Settings.Address = bookingdetail;
                //Cardle.Data.Settings.DistanceTravelled = Convert.ToString(distance);
                //MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position((location.Latitude + destination.Latitude) / 2, (location.Longitude + destination.Longitude) / 2), Distance.FromKilometers(5)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }

        private void BookingDate_DateSelected(object sender, DateChangedEventArgs e)
        {

        }
        private void ToTextChange(object sender, EventArgs e)
        {
            
            UpdateMap();
        }
        private void FromTextChange(object sender, EventArgs e)
        {
            UpdateMap();
        }

        private async void SelectPayment(object sender, EventArgs e)
        {
            if(txtFromLocation.Text!= null && txtToLocation.Text != null)
            {
                User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
                Booking book = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.Address);
                string to = book.EndOfDest;
                string from = book.StartOfDest;
                double distance = book.distance;
                double fare = Convert.ToDouble(distance) * 2;
                string action = await DisplayActionSheet("Select Payment", "Cancel", null, "Cash", "PayLah");
                Cardle.Data.Settings.PaymentType = action;
                this.BindingContext = new
                {
                    To = to,
                    From = from,
                    payment = action,
                    points = "Current Points: " + Convert.ToString(user.Rewards),
                    price = $"${fare:0.00}"
                };
                Cardle.Data.Settings.Price = Convert.ToString(fare);
                //var bookingdetails = new Booking
                //{
                //    StartOfDest = to,
                //    EndOfDest = from,
                //    distance = distance
                //};
                //string bookingdetail = JsonConvert.SerializeObject(bookingdetails);
                //Cardle.Data.Settings.Address = bookingdetail;
            }
            else
            {
                await DisplayAlert("Error", "Please select your destination first", "Ok");
            }
        }

        private async void ClaimReward(object sender, EventArgs e)
        {
            Booking book = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.Address);
            string result = await DisplayPromptAsync("Fare Discount", "Points", maxLength: 3, keyboard: Keyboard.Numeric);
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            string action = Cardle.Data.Settings.PaymentType;
            if (user.Rewards > Convert.ToInt32(result))
            {
                double discount_use = Convert.ToInt32(result) * 0.01;
                string to = book.EndOfDest;
                string from = book.StartOfDest;
                double distance = book.distance;
                double fare = Convert.ToDouble(distance) * 2;
                double afterdiscount = fare - discount_use;
                int rewards_left = user.Rewards - Convert.ToInt32(result);
                this.BindingContext = new
                {
                    To = to,
                    From = from,
                    payment = action,
                    points = $"Current Points: {rewards_left}",
                    price = $"${afterdiscount:0.00}"
                };
                Cardle.Data.Settings.Price = Convert.ToString(afterdiscount);
                await firebaseHelper.UpdateUserReward(user.ID, user.Username, user.Password, user.EmailAddress, user.Role, rewards_left);
            }
            else
            {
                await DisplayAlert("Error", "You do not have enough points", "Ok");
            }
        }
    }
}