﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Cardle.Data
{
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

		#endregion


		public static string UserID
		{
			get
			{
				return AppSettings.GetValueOrDefault("BookingsKey", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("BookingsKey", value);
			}
		}

        public static string BookingID
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }
		public static string DriverID
		{
			get
			{
				return AppSettings.GetValueOrDefault("DriverID", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("DriverID", value);
			}
		}
        public static string ReceiverID
        {
            get
            {
                return AppSettings.GetValueOrDefault("ReceiverKey", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("ReceiverKey", value);
            }
        }
		public static string PaymentType
		{
			get
			{
				return AppSettings.GetValueOrDefault("Action", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("Action", value);
			}
		}
		public static string DistanceTravelled
		{
			get
			{
				return AppSettings.GetValueOrDefault("Distance", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("Distance", value);
			}
		}
		public static string FromAddress
		{
			get
			{
				return AppSettings.GetValueOrDefault("From", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("From", value);
			}
		}
		public static string ToAddress
		{
			get
			{
				return AppSettings.GetValueOrDefault("To", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("To", value);
			}
		}
		public static string Address
		{
			get
			{
				return AppSettings.GetValueOrDefault("Address", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("Address", value);
			}
		}
		public static string Price
		{
			get
			{
				return AppSettings.GetValueOrDefault("Price", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("Price", value);
			}
		}
		public static string Subscription
		{
			get
			{
				return AppSettings.GetValueOrDefault("Subscription", SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue("Subscription", value);
			}
		}
	}
}
