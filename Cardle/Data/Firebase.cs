﻿using System;
using System.Collections.Generic;
using System.Text;
using Cardle.Models;
using Firebase.Database;
using Firebase.Database.Query;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Cardle.Data
{
	public class Firebase
	{
		FirebaseClient firebase = new FirebaseClient("https://cardle-a04b1.firebaseio.com/");

       
        public async Task<List<User>> GetAllUser() //This is to read all the data from the firebase
		{

			return (await firebase
			  .Child("User")
			  .OnceAsync<User>()).Select(item => new User
			  {
				  ID = item.Object.ID,
				  Username = item.Object.Username,
                  EmailAddress = item.Object.EmailAddress,
				  Password = item.Object.Password,
                  Role = item.Object.Role,
                  Rewards = item.Object.Rewards,
                  Subscription = item.Object.Subscription,
                  QRCode = item.Object.QRCode,
                  Verified = item.Object.Verified

              }).ToList();
		}
        public async Task<User> GetUser(string username, string password)// To Retreive specific user from the alluser list
        {
            var allusers = await GetAllUser();
            await firebase
              .Child("User")
              .OnceAsync<User>();
            return allusers.Where(a => a.Username == username || a.Password == password).FirstOrDefault();
        }
        public async Task<User> GetUserData(int id)// To Retreive specific user from the alluser list
        {
            var allusers = await GetAllUser();
            await firebase
              .Child("User")
              .OnceAsync<User>();
            return allusers.Where(a => a.ID == id).FirstOrDefault();
        }

        // This is to register the new user and add it into Firebase
        public async Task AddUser(int id, string username, string EmailAddress, string password, string role, int reward, bool sub, bool val)
		{
			
			await firebase
			  .Child("User")
			  .PostAsync(new User() {ID=id, Username = username, EmailAddress = EmailAddress, Password = password, Role = role, Rewards = reward, Subscription = sub, Verified = val});
		}
		public async Task UpdateUserProfile(int id,string username, string password, string emailAddress, string role)
		{
			var toUpdateUser = (await firebase
			  .Child("User")
			  .OnceAsync<User>()).Where(a => a.Object.ID == id).FirstOrDefault();

			await firebase
			  .Child("User")
			  .Child(toUpdateUser.Key)
			  .PutAsync(new User() {ID=id, Username = username, Password=password, EmailAddress = emailAddress , Role = role });
		}
        public async Task UpdateUserReward(int id, string username, string password, string emailAddress, string role, int reward)
        {
            var toUpdateUser = (await firebase
              .Child("User")
              .OnceAsync<User>()).Where(a => a.Object.ID == id).FirstOrDefault();

            await firebase
              .Child("User")
              .Child(toUpdateUser.Key)
              .PutAsync(new User() { ID = id, Username = username, EmailAddress = emailAddress, Password = password, Role = role, Rewards = reward });
        }
        public async Task UpdateUserSubscription(int id, string username, string password, string emailAddress, string role, int reward, bool sub)
        {
            var toUpdateUser = (await firebase
              .Child("User")
              .OnceAsync<User>()).Where(a => a.Object.ID == id).FirstOrDefault();

            await firebase
              .Child("User")
              .Child(toUpdateUser.Key)
              .PutAsync(new User() { ID = id, Username = username, EmailAddress = emailAddress, Password = password, Role = role, Rewards = reward, Subscription = sub });
        }
        public async Task AddBooking( string name, int bookingid, string startofdest, string endofdest, DateTime tripdate, TimeSpan triptime, int driverid, int riderid, string status)
        {
            await firebase
              .Child("Booking")
              .PostAsync(new Booking() {Name = name, BookingID = bookingid, StartOfDest = startofdest, EndOfDest = endofdest, TripDate = tripdate, TripTime = triptime, DriverID = driverid, RiderID = riderid, Status = status});
        }
        public async Task<List<Booking>> GetAllBooking() //This is to read all the data from the firebase
        {

            return (await firebase
              .Child("Booking")
              .OnceAsync<Booking>()).Select(item => new Booking
              {
                  Name = item.Object.Name,
                  BookingID = item.Object.BookingID,
                  StartOfDest = item.Object.StartOfDest,
                  EndOfDest = item.Object.EndOfDest,
                  TripDate = item.Object.TripDate,
                  TripTime = item.Object.TripTime,
                  DriverID = item.Object.DriverID,
                  RiderID = item.Object.RiderID,
                  Status = item.Object.Status


              }).ToList();

        }
        public async Task UpdateBookings(string name, int bookingid, string startofdest, string endofdest, DateTime tripdate, TimeSpan triptime, int driverid, int riderid, string status)
        {
            var toUpdateUser = (await firebase
              .Child("Booking")
              .OnceAsync<Booking>()).Where(a => a.Object.BookingID == bookingid).FirstOrDefault();

            await firebase
              .Child("Booking")
              .Child(toUpdateUser.Key)
              .PutAsync(new Booking() { Name = name, BookingID = bookingid, StartOfDest = startofdest, EndOfDest = endofdest, TripDate = tripdate, TripTime = triptime, DriverID = driverid, RiderID = riderid, Status = status});
        }
        public async Task AddTrip(int tripid,string name, int bookingid, int driverid, int riderid)
        {
            await firebase
              .Child("Trip")
              .PostAsync(new Trip() {TripID = tripid, Name = name, BookingID = bookingid, DriverID = driverid, RiderID = riderid});
        }
        public async Task<List<Trip>> GetAllTrip() //This is to read all the data from the firebase
        {

            return (await firebase
              .Child("Trip")
              .OnceAsync<Trip>()).Select(item => new Trip
              {
                  TripID = item.Object.TripID,
                  Name = item.Object.Name,
                  BookingID = item.Object.BookingID,
                  DriverID = item.Object.DriverID,
                  RiderID = item.Object.RiderID,
                  PassengerRating = item.Object.PassengerRating,
                  DriverRating = item.Object.DriverRating



              }).ToList();

        }
        public async Task UpdateTrip(int tripid, string name, int bookingid, int driverid, int riderid, string passengerrating, string driverrating)
        {
            var toUpdateUser = (await firebase
              .Child("Trip")
              .OnceAsync<Trip>()).Where(a => a.Object.TripID == tripid).FirstOrDefault();

            await firebase
              .Child("Trip")
              .Child(toUpdateUser.Key)
              .PutAsync(new Trip() { TripID = tripid, Name = name, BookingID = bookingid, DriverID = driverid, RiderID = riderid, PassengerRating = passengerrating, DriverRating = driverrating });
        }
        public async Task<List<Whitelist>> GetAllWhitelistUser() //This is to read all the data from the firebase
        {
            return (await firebase
              .Child("Whitelist")
              .OnceAsync<Whitelist>()).Select(item => new Whitelist
              {
                  UserID = item.Object.UserID,
                  DriverID = item.Object.DriverID
              }).ToList();
        }
        public async Task<Whitelist> GetWhitelist(int userid, int driverid)// To Retreive specific user from the alluser list
        {
            var allusers = await GetAllWhitelistUser();
            await firebase
              .Child("Whitelist")
              .OnceAsync<Whitelist>();
            return allusers.Where(a => a.UserID == userid || a.DriverID == driverid).FirstOrDefault();
        }
        public async Task<List<Blacklist>> GetAllBlackListUser()
        {
            return (await firebase
              .Child("Blacklist")
              .OnceAsync<Blacklist>()).Select(item => new Blacklist
              {
                  UserID = item.Object.UserID,
                  DriverID = item.Object.DriverID
              }).ToList();
        }
        public async Task<Blacklist> GetBlackist(int userid, int driverid)// To Retreive specific user from the alluser list
        {
            var allusers = await GetAllBlackListUser();
            await firebase
              .Child("Blacklist")
              .OnceAsync<Blacklist>();
            return allusers.Where(a => a.UserID == userid || a.DriverID == driverid).FirstOrDefault();
        }
        public async Task AddWhitelistUser(int userid, int driverid)
        {
            await firebase
              .Child("Whitelist")
              .PostAsync(new Whitelist() { UserID = userid, DriverID = driverid });
        }
        public async Task AddBlacklistUser(int userid, int driverid)
        {
            await firebase
              .Child("Blacklist")
              .PostAsync(new Whitelist() { UserID = userid, DriverID = driverid });
        }
        public async Task<List<Interest>> GetAllInterest()
        {
            return (await firebase
              .Child("Interest")
              .OnceAsync<Interest>()).Select(item => new Interest
              {
                  UserID = item.Object.UserID,
                  Ethics = item.Object.Ethics,
                  Geography = item.Object.Geography,
                  Gaming = item.Object.Gaming,
                  History = item.Object.History,
                  Literature = item.Object.Literature,
                  Philosophy = item.Object.Philosophy,
                  Politics = item.Object.Politics,
                  Sports = item.Object.Sports,
                  Science = item.Object.Science,
                  Technology = item.Object.Technology,
                  OptOut = item.Object.OptOut
              }).ToList();
        }
        public async Task<Interest> GetInterest(int userid)// To Retreive specific user from the alluser list
        {
            var allusers = await GetAllInterest();
            await firebase
              .Child("Interest")
              .OnceAsync<Interest>();
            return allusers.Where(a => a.UserID == userid).FirstOrDefault();
        }
        public async Task AddInterest(int userid, bool ethics, bool geography, bool gaming, bool history, bool literature, bool philosophy, bool psychology, bool politics, bool sports, bool science, bool technology, bool optout)
        {
            await firebase
              .Child("Interest")
              .PostAsync(new Interest() { UserID = userid, Ethics = ethics, Geography = geography, Gaming = gaming, History = history, Literature = literature, Philosophy = philosophy, Psychology = psychology, Politics = politics, Sports = sports, Science = science, Technology = technology, OptOut = optout });
        }
        public async Task UpdateInterest(int userid, bool ethics, bool geography, bool gaming, bool history, bool literature, bool philosophy, bool psychology, bool politics, bool sports, bool science, bool technology, bool optout)
        {
            var toUpdateUser = (await firebase
              .Child("Interest")
              .OnceAsync<Interest>()).Where(a => a.Object.UserID == userid).FirstOrDefault();

            await firebase
              .Child("Interest")
              .Child(toUpdateUser.Key)
              .PutAsync(new Interest() { UserID = userid, Ethics = ethics, Geography = geography, Gaming = gaming, History = history, Literature = literature, Philosophy = philosophy, Psychology = psychology, Politics = politics, Sports = sports, Science = science, Technology = technology, OptOut = optout });
        }
        public async Task AddMessage(int sender, int receiver, string sendername, string message)
        {
            await firebase
              .Child("Message")
              .PostAsync(new Message() { Sender = sender, Receiver = receiver, SenderName = sendername, Msg = message});
        }
        public async Task<List<Message>> GetAllMessage() //This is to read all the data from the firebase
        {

            return (await firebase
              .Child("Message")
            
              .OnceAsync<Message>()).Select(item => new Message
              {
                  Sender = item.Object.Sender,
                  SenderName = item.Object.SenderName,
                  Receiver = item.Object.Receiver,
                  Msg = item.Object.Msg
              }).ToList();

        }
    }
}
