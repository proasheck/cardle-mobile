﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardle.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;

namespace Cardle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AccountEdit : ContentPage
	{
		Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
		public AccountEdit()
		{
			InitializeComponent();
			User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
			txtID.Text = Convert.ToString(user.ID);
			txtUsername.Text = user.Username;
			txtPassword.Text = user.Password;
			txtEmailAddress.Text = user.EmailAddress;
		}

		private async void Update_Profile(object sender, EventArgs e)
		{
            if (pickRole.SelectedItem.ToString() == "Are you a Driver or a Rider")
            {
                await DisplayAlert("Error", "Please Choose a Role", "OK");
            }
            else
            {
                await firebaseHelper.UpdateUserProfile(Convert.ToInt32(txtID.Text), txtUsername.Text, txtPassword.Text, txtEmailAddress.Text, pickRole.Title);
                await DisplayAlert("Success", "User Updated Successfully", "OK");
                await Navigation.PushAsync(new Home { });
            }
            
		}
	}
}