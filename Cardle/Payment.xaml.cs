﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cardle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Payment
	{
		public Payment()
		{
			InitializeComponent();
			string price = Cardle.Data.Settings.Price;
			double fare = Convert.ToDouble(price);
			this.BindingContext = new
			{
				Price = $"Please scan the QR Code Below and pay ${fare:0.00}"
			};
		}
	}
}