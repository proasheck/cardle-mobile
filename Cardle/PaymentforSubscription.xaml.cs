﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Cardle.Models;

namespace Cardle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PaymentforSubscription
	{
		Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
		User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
		public PaymentforSubscription()
		{
			InitializeComponent();
		}
		private async void PaySubscription(object sender, EventArgs e)
		{
			if (Account != null && Name != null)
			{
				bool subscription = true;
				Cardle.Data.Settings.Subscription = Convert.ToString(subscription);
				await firebaseHelper.UpdateUserSubscription(user.ID, user.Username, user.Password, user.EmailAddress, user.Role, user.Rewards, subscription);
				await DisplayAlert("Success", "Check the page later to see your new Subscription", "OK");
				await PopupNavigation.Instance.PopAsync();
				var contact = new User //sessioning purpose
				{
					ID = user.ID,
					Username = user.Username,
					EmailAddress = user.EmailAddress,
					Password = user.Password,
					Role = user.Role,
					Rewards = user.Rewards,
					Subscription = true
				};
				string detail = JsonConvert.SerializeObject(contact);
				Cardle.Data.Settings.UserID = detail;
				await Navigation.PushAsync(new Subscription());
			}
			else
			{
				await DisplayAlert("Error", "Please fill in the your name and account number", "Ok");
			}
		}
	}
}