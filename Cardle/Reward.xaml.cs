﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardle.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;

namespace Cardle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Reward : ContentPage
	{
		Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
		public IList<User> User { get; set; }
		public Reward()
		{
			User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
			InitializeComponent();
			User = new List<User>();
			points.Text = "Current Points: " + Convert.ToString(user.Rewards);
		}
		protected async override void OnAppearing()
		{
			base.OnAppearing();
			User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

			var allusers = await firebaseHelper.GetAllUser();

			foreach(User u in allusers)
			{
				if (user.ID != u.ID && u.Role=="Rider")
				{
					User.Add(new User
					{
						ID = u.ID,
						Username = u.Username,
						EmailAddress = u.Username,
						Password = u.Password,
						Role = u.Role,
						Rewards = u.Rewards
					});
				}
			}
			BindingContext = this;
		}
		private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			User selectedItem = e.SelectedItem as User;
			User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
			string result = await DisplayPromptAsync("Sharing Rewards", "Points Sharing", maxLength: 3, keyboard: Keyboard.Numeric);
			int reward = Convert.ToInt32(result);
			if(reward < user.Rewards)
			{
				int pointsgain = selectedItem.Rewards + reward;
				int pointsdeduct = user.Rewards - reward;
				await firebaseHelper.UpdateUserReward(selectedItem.ID, selectedItem.Username, selectedItem.Password, selectedItem.EmailAddress, selectedItem.Role, pointsgain);
				await firebaseHelper.UpdateUserReward(user.ID, user.Username, user.Password, user.EmailAddress, user.Role, pointsdeduct);
				await DisplayAlert("Success", $"You have successfuly give {reward} points to {selectedItem.Username}","Ok");
			}
			else
			{
				await DisplayAlert("Error", "You do not have enough points", "Ok");
			}
		}
	}
}