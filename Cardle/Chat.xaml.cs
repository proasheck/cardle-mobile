﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;

using Cardle.Models;

namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Chat : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public IList<Message> Chats{ get; private set; }
        public Chat()
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            User receiver = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.ReceiverID);
            InitializeComponent();
            Chats = new List<Message>();

        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            User receiver = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.ReceiverID);

            var allMessages = await firebaseHelper.GetAllMessage();
            foreach (Message i in allMessages)
            {
                if ((i.Receiver == user.ID && i.Sender == receiver.ID) ||(i.Receiver == receiver.ID && i.Sender == user.ID))
                {
                    Chats.Add(new Message
                    {
                        Receiver = i.Receiver,
                        Sender = i.Sender,
                        SenderName  = i.SenderName,
                        Msg = i.Msg
                    });
                }

            }
            BindingContext = this;
        }
        async void SendButtonClicked(object sender, EventArgs e)
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            User receiver = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.ReceiverID);
            if(txtMsg.Text != "")
            {
                
                await firebaseHelper.AddMessage(user.ID, receiver.ID, user.Username, txtMsg.Text);
                txtMsg.Text = string.Empty;
            }
            else
            {
                await DisplayAlert("Error", "There is no Message", "OK");
            }
        }
    }
}