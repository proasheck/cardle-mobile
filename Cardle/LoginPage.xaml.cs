﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cardle.Data;
using Cardle.Models;
using Newtonsoft.Json;

namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public LoginPage()
        {
            InitializeComponent();
        }
        protected async override void OnAppearing()
        {

            base.OnAppearing();
            var allUsers = await firebaseHelper.GetAllUser();
        }
        private async void LoginPageButtonClicked(object sender, EventArgs e)
        {
            if (txtPassword.Text != "" && txtUsername.Text != "")
            {
                var user = await firebaseHelper.GetUser(txtUsername.Text, txtPassword.Text);
				if (user != null && user.Username == txtUsername.Text)
				{
					if (user.Password == txtPassword.Text)
					{
                        if(user.Verified == true || user.Role == "Rider")
                        {
                            var contact = new User //sessioning purpose
                            {
                                ID = user.ID,
                                Username = user.Username,
                                EmailAddress = user.EmailAddress,
                                Password = user.Password,
                                Role = user.Role,
                                Rewards = user.Rewards,
                                Subscription = user.Subscription
                            };
                            await DisplayAlert("Welcome", $"{txtUsername.Text} to Cardle", "OK");
                            string person = JsonConvert.SerializeObject(contact);
                            //Cardle.Data.Settings.UserID = Convert.ToString(contact.ID);
                            Cardle.Data.Settings.UserID = person;
                            var secondPage = new MasterDetail();
                            secondPage.BindingContext = contact;
                            //var thirdPage = new ViewBooking();
                            //thirdPage.BindingContext = contact;
                            await Navigation.PushAsync(secondPage);
                            //await Navigation.PushAsync(new MasterDetail
                            //{
                            //});
                        }
                        else
                        {
                            await DisplayAlert("Error", $"Contact Cardle@mail.com to be Verified ", "OK");

                        }

                    }
					else
					{
						await DisplayAlert("Error", $"Username or Password Incorrect", "OK");

					}
				}
                else
                {
                    await DisplayAlert("Error", $"Username not found!", "OK");

                }
            }
            else
            {
                await DisplayAlert("Error", $"Username or Password Empty", "OK");

            }
        }
        async void RegisterButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterPage
            {
            });
        }
    }
}