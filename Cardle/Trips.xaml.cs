﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardle.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;


namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Trips : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public IList<Trip> Trip { get; private set; }
        public Trips()
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

            InitializeComponent();
            Trip = new List<Trip>();
        }
        protected async override void OnAppearing()
        {

            base.OnAppearing();
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            if (user.Role == "Driver")
            {
                var allTrips = await firebaseHelper.GetAllTrip();

                foreach (Trip i in allTrips)
                {
                    if (i.DriverID == user.ID)
                    {
                        Trip.Add(new Trip
                        {
                            Name = i.Name,
                            BookingID = i.BookingID,
                            DriverID = i.DriverID,
                            RiderID = i.RiderID,
                            PassengerRating = i.PassengerRating,
                            DriverRating = i.DriverRating,
                            IsVisible=false


                        });;
                    }                   
                }
                BindingContext = this;

            }
            else  if (user.Role == "Rider")
            {
                var allTrips = await firebaseHelper.GetAllTrip();

                foreach (Trip i in allTrips)
                {
                    if (i.RiderID == user.ID)
                    {
                        Trip.Add(new Trip
                        {
                            Name = i.Name,
                            BookingID = i.BookingID,
                            DriverID = i.DriverID,
                            RiderID = i.RiderID,
                            PassengerRating = i.PassengerRating,
                            DriverRating = i.DriverRating,
                            IsVisible = true

                        }); ;
                    }
                }               
                BindingContext = this;
            }
        }


        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Trip selectedItem = e.SelectedItem as Trip;

            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

            Cardle.Data.Settings.DriverID = Convert.ToString(selectedItem.DriverID);

            string action = await DisplayActionSheet("Rating","Cancel","Ok" ,"Very Poor", "Poor", "Neutral", "Good", "Excellent");
            if(action == "Cancel" || action == "Ok")
            {

            }
            else
            {
                string rating = action;
                if (user.Role == "Driver" && user.ID == selectedItem.DriverID)
                {
                    await firebaseHelper.UpdateTrip(selectedItem.TripID, selectedItem.Name, selectedItem.BookingID, selectedItem.DriverID, selectedItem.RiderID, selectedItem.PassengerRating, rating);
                    await DisplayAlert("Success", "Rating has been set", "OK");
                    await Navigation.PushAsync(new Trips { });

                }
                else if (user.Role == "Rider" && user.ID == selectedItem.RiderID)
                {
                    await firebaseHelper.UpdateTrip(selectedItem.TripID, selectedItem.Name, selectedItem.BookingID, selectedItem.DriverID, selectedItem.RiderID, rating, selectedItem.DriverRating);
                    await DisplayAlert("Success", "Rating has been set", "OK");
                    await Navigation.PushAsync(new Trips { });


                }
                else
                {
                    await DisplayAlert("Error", "You are not allowed to give rating", "OK");

                }
            }
        }

        private async void BlockDriverClicked(object sender, EventArgs e)
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            int id = Convert.ToInt32(Cardle.Data.Settings.DriverID);
            if(await firebaseHelper.GetBlackist(user.ID, id)!=null)
            {
                await DisplayAlert("Error", "Driver has been blocked", "OK");
            }
            else
            {
                await firebaseHelper.AddBlacklistUser(user.ID, id);
                await DisplayAlert("Success", "You have blocked the driver", "OK");
            }
        }

        private async void PrioritiseDriverClicked(object sender, EventArgs e)
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            int id = Convert.ToInt32(Cardle.Data.Settings.DriverID);
            if (await firebaseHelper.GetWhitelist(user.ID, id) != null)
            {
                await DisplayAlert("Error", "You have added this driver to your favourites", "OK");
            }
            else
            {
                await firebaseHelper.AddWhitelistUser(user.ID, id);
                await DisplayAlert("Success", "Driver has been placed in your favourites", "OK");
            }
        }
    }
}