﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cardle.Data;
using Cardle.Models;

namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage 
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public RegisterPage()
        {
            InitializeComponent();
            Application.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, "#c6c0e4");
        }
        protected async override void OnAppearing()
        {

            base.OnAppearing();
            var allUsers = await firebaseHelper.GetAllUser();
        }
        // Registration Button Clicked
        private async void RegisterPageButtonClicked(object sender, EventArgs e)
        {
			var allUsers = await firebaseHelper.GetAllUser();
			int x = 0;
			foreach (var i in allUsers)
			{
				if (Id != null)
				{
					x += 2;
				}
			}
			int ID = x;

			if (txtPassword.Text == "" || txtUsername.Text == "")
            {
                await DisplayAlert("Error", "Username or Password is empty", "OK");
            }
            else if (pickRole.SelectedItem.ToString() == "Are you a Driver or a Rider")
            {
                await DisplayAlert("Error", "Please Choose a Role", "OK");
            }
            else if ( txtConfirmPassword.Text == txtPassword.Text && (pickRole.SelectedItem.ToString() == "Rider" || pickRole.SelectedItem.ToString() == "Driver"))
            {
                if(pickRole.SelectedItem.ToString() == "Rider")
                {
                    await firebaseHelper.AddUser(ID, txtUsername.Text, txtEmailAddress.Text, txtPassword.Text, pickRole.SelectedItem.ToString(), 0, false, true);
                    txtUsername.Text = string.Empty;
                    txtEmailAddress.Text = string.Empty;
                    txtPassword.Text = string.Empty;
                    txtConfirmPassword.Text = string.Empty;
                    await DisplayAlert("Success", "User Added Successfully", "OK");
                    await Navigation.PushAsync(new LoginPage
                    {
                    });
                }
                else if (pickRole.SelectedItem.ToString() == "Driver")
                {
                    await firebaseHelper.AddUser(ID, txtUsername.Text, txtEmailAddress.Text, txtPassword.Text, pickRole.SelectedItem.ToString(), 0, false, false);
                    txtUsername.Text = string.Empty;
                    txtEmailAddress.Text = string.Empty;
                    txtPassword.Text = string.Empty;
                    txtConfirmPassword.Text = string.Empty;
                    await DisplayAlert("Success", "User Added Successfully", "OK");
                    await Navigation.PushAsync(new LoginPage
                    {
                    });
                }
                
            }
            else
            {
                await DisplayAlert("Error", "Password and Confirm Password does not match", "OK");
            }
            
        }
    }
}