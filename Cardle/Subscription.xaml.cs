﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cardle.Models;
using Newtonsoft.Json;

namespace Cardle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Subscription : ContentPage
	{
		Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
		User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
		public Subscription()
		{
			InitializeComponent();
			if (user.Subscription == true)
			{
				this.BindingContext = new
				{
					Name = user.Username,
					Subscription = "Premium"
				};
			}
			else
			{
				this.BindingContext = new
				{
					Name = user.Username,
					Subscription = "Basic"
				};
			}
		}

		private async void ChooseSubscription(object sender, EventArgs e)
		{			
			string action = await DisplayActionSheet("Do you want Subscription?", "No", "Yes");
			if (user.Subscription==true) 
			{
				await DisplayAlert("Error", "You have already subscribed", "Ok");
			}
			else if (action == "Yes")
			{
				await PopupNavigation.Instance.PushAsync(new PaymentforSubscription());
				//bool subscription = true;
				//await firebaseHelper.UpdateUserSubscription(user.ID, user.Username, user.Password, user.EmailAddress, user.Role, user.Rewards, subscription);
				//bool result = Convert.ToBoolean(Cardle.Data.Settings.Subscription);
				//if (result == true)
				//{
				//	this.BindingContext = new
				//	{
				//		Subscription = "Premium"
				//	};
				//	await PopupNavigation.Instance.PopAsync();
				//}
			}
			else
			{

			}
		}
	}
}