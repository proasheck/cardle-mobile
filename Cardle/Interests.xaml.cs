﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Xamarin.Forms.Maps;
using Cardle.Models;
using Xamarin.Essentials;

namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Interests : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();

        public Interests()
        {
            InitializeComponent();
            OnAppearing();


        }
        protected async override void OnAppearing()
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

            var allInterest = await firebaseHelper.GetAllInterest();

            int exist = 0;
            foreach (var i in allInterest)
            {
                if (user.ID == i.UserID)
                {
                    exist = 1;
                }
            }
            if(exist == 1)
            {
                var Interest = await firebaseHelper.GetInterest(user.ID);
                ethics.IsChecked = Interest.Ethics;
                geography.IsChecked = Interest.Geography;
                gaming.IsChecked = Interest.Gaming;
                history.IsChecked = Interest.History;
                literature.IsChecked = Interest.Literature;
                philosophy.IsChecked = Interest.Philosophy;
                gaming.IsChecked = Interest.Gaming;
                history.IsChecked = Interest.History;
                literature.IsChecked = Interest.Literature;
                philosophy.IsChecked = Interest.Philosophy;
                politics.IsChecked = Interest.Politics;
                sports.IsChecked = Interest.Sports;
                science.IsChecked = Interest.Science;
                technology.IsChecked = Interest.Technology;
                optout.IsChecked = Interest.OptOut;

            }
        }


            private async void Button_Clicked(object sender, EventArgs e)
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            var allInterest = await firebaseHelper.GetAllInterest();
            int exist = 0;
            foreach (var i in allInterest)
            {
                if (user.ID == i.UserID)
                {
                    exist = 1;
                }
            }
            if (exist == 0)
            {
                await DisplayAlert("Success", "Interest Successfully added", "OK");

                await firebaseHelper.AddInterest(user.ID, ethics.IsChecked, geography.IsChecked, gaming.IsChecked, history.IsChecked, literature.IsChecked, philosophy.IsChecked, psychology.IsChecked, politics.IsChecked, sports.IsChecked, science.IsChecked, technology.IsChecked, optout.IsChecked);
            }
            else
            {
                await DisplayAlert("Success", "Interest Updated Succesfully", "OK");

                await firebaseHelper.UpdateInterest(user.ID, ethics.IsChecked, geography.IsChecked, gaming.IsChecked, history.IsChecked, literature.IsChecked, philosophy.IsChecked, psychology.IsChecked, politics.IsChecked, sports.IsChecked, science.IsChecked, technology.IsChecked, optout.IsChecked);
            }
        }
    }
}