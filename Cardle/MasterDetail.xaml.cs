﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardle.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cardle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterDetail : MasterDetailPage
	{
		public List<MasterPageItem> menulist { get; set; }
		public MasterDetail()
		{
			Application.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, "#c6c0e4");
			InitializeComponent();

			Application.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, "#c6c0e4");
			Detail = new NavigationPage(new Home());

			IsPresented = false;

			menulist = new List<MasterPageItem>();

			var home = new MasterPageItem() { Title = "Home", Icon = "baseline_home_black_48.png", TargetType = typeof(Home) };
			var Profile = new MasterPageItem() { Title = "Profile Page", Icon = "baseline_person_black_48.png", TargetType = typeof(AccountEdit) };
			var Bookings = new MasterPageItem() { Title = "Bookings", Icon = "baseline_schedule_black_48.png", TargetType = typeof(Bookings) };
			var Trips = new MasterPageItem() { Title = "Trips", Icon = "baseline_directions_car_black_48.png", TargetType = typeof(Trips) };
			var interest = new MasterPageItem() { Title = "Interest", Icon = "baseline_emoji_events_black_48.png", TargetType = typeof(Interests) };
			var reward = new MasterPageItem() { Title = "Share Rewards", Icon = "baseline_card_giftcard_black_48.png", TargetType = typeof(Reward) };
			var subscription = new MasterPageItem() { Title = "Subscription", Icon = "baseline_attach_money_black_48.png", TargetType = typeof(Subscription) };
            var chat = new MasterPageItem() { Title = "Chat", Icon = "baseline_chat_black_48.png", TargetType = typeof(ChatList) };
            var logout = new MasterPageItem() { Title = "Logout", Icon = "baseline_exit_to_app_black_48.png", TargetType = typeof(Login) };

			menulist.Add(home);
			menulist.Add(Profile);
			menulist.Add(Bookings);
			menulist.Add(Trips);
			menulist.Add(interest);
			menulist.Add(reward);
			menulist.Add(subscription);
            menulist.Add(chat);
            menulist.Add(logout);

			list.ItemsSource = menulist;

			Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(Home)));

			this.BindingContext = new
			{
				Header = "",
				Image = "Header.png",
				Footer = "Welcome to Cardle!"
			};
		}
		private async void OnMenuSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = (MasterPageItem)e.SelectedItem;
			Type page = item.TargetType;

			if(item.Title!="Logout")
			{
				Detail = new NavigationPage((Page)Activator.CreateInstance(page));
				IsPresented = false;
			}
			else
			{
				bool result = await DisplayAlert("Logout", "Are you sure want to log out?", "Yes", "No");
				if (result == true)
				{
					Detail = new NavigationPage((Page)Activator.CreateInstance(page));
					IsPresented = false;
				}
				else
				{
					IsPresented = false;
				}
			}
		}
		//private void Home(object sender, EventArgs e)
		//{
		//	Detail = new NavigationPage(new Home());

		//	IsPresented = false;
		//}

		//private void Profile_Page(object sender, EventArgs e)
		//{
		//	Detail = new NavigationPage(new AccountEdit());

		//	IsPresented = false;
		//}

  //      private void Bookings(object sender, EventArgs e)
  //      {
  //          Detail = new NavigationPage(new Bookings());

  //          IsPresented = false;
  //      }
  //      private void Trips(object sender, EventArgs e)
  //      {
  //          Detail = new NavigationPage(new Trips());

  //          IsPresented = false;
  //      }
  //      private void Interests(object sender, EventArgs e)
  //      {
  //          Detail = new NavigationPage(new Interests());

  //          IsPresented = false;
  //      }
		//private void Reward(object sender, EventArgs e)
		//{
		//	Detail = new NavigationPage(new Reward());

		//	IsPresented = false;
		//}
	}
}