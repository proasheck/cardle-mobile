﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardle.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;



namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Bookings : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public IList<Booking> Booking { get; private set; }
        public Bookings()
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            InitializeComponent();
            Booking = new List<Booking>();
            
        }
        protected async override void OnAppearing()
        {

            base.OnAppearing();
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            if (user.Role == "Driver")
            {
                var allBookings = await firebaseHelper.GetAllBooking();

                foreach (Booking i in allBookings)
                {
                    if((i.Status == "pending" || i.DriverID == user.ID) && i.Status != "Completed" )
                    {
                        var blacklist = await firebaseHelper.GetBlackist(i.RiderID, user.ID);
                        var whitelist = await firebaseHelper.GetWhitelist(i.RiderID, user.ID);
                        if (blacklist == null && whitelist == null)
                        {
                            Booking.Add(new Booking
                            {
                                Name = i.Name,
                                BookingID = i.BookingID,
                                StartOfDest = i.StartOfDest,
                                EndOfDest = i.EndOfDest,
                                TripDate = i.TripDate,
                                TripTime = i.TripTime,
                                DriverID = i.DriverID,
                                RiderID = i.RiderID,
                                Status = i.Status

                            });
                        }
                        else if (whitelist!=null && whitelist.DriverID == user.ID && whitelist.UserID == i.RiderID)
                        {
                            Booking.Add(new Booking
                            {
                                Name = i.Name,
                                BookingID = i.BookingID,
                                StartOfDest = i.StartOfDest,
                                EndOfDest = i.EndOfDest,
                                TripDate = i.TripDate,
                                TripTime = i.TripTime,
                                DriverID = i.DriverID,
                                RiderID = i.RiderID,
                                Status = i.Status

                            });
                        }
                        else if(blacklist!=null)
                        {
                            if (blacklist.DriverID != user.ID || blacklist.UserID != i.RiderID)
                            {
                                Booking.Add(new Booking
                                {
                                    Name = i.Name,
                                    BookingID = i.BookingID,
                                    StartOfDest = i.StartOfDest,
                                    EndOfDest = i.EndOfDest,
                                    TripDate = i.TripDate,
                                    TripTime = i.TripTime,
                                    DriverID = i.DriverID,
                                    RiderID = i.RiderID,
                                    Status = i.Status

                                });
                            }
                        }
                    }

                }
                BindingContext = this;
            }
            if(user.Role == "Rider")
            {
                var allBookings = await firebaseHelper.GetAllBooking();

                foreach (Booking i in allBookings)
                {
                    if(i.RiderID == user.ID && i.Status != "Completed")
                    {
                        Booking.Add(new Booking
                        {
                            Name = i.Name,
                            BookingID = i.BookingID,
                            StartOfDest = i.StartOfDest,
                            EndOfDest = i.EndOfDest,
                            TripDate = i.TripDate,
                            TripTime = i.TripTime,
                            DriverID = i.DriverID,
                            RiderID = i.RiderID,
                            Status = i.Status

                        });
                    }                   
                }
                BindingContext = this;
            }

        }


        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            

            Booking selectedItem = e.SelectedItem as Booking;
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

            string person = JsonConvert.SerializeObject(user);
            //Cardle.Data.Settings.UserID = Convert.ToString(contact.ID);
            Cardle.Data.Settings.UserID = person;
            //Access Token
            string selectedBooking = JsonConvert.SerializeObject(selectedItem);
            //Cardle.Data.Settings.UserID = Convert.ToString(contact.ID);
            Cardle.Data.Settings.BookingID = selectedBooking;
            var secondPage = new ViewBooking();
            secondPage.BindingContext = selectedBooking;
            secondPage.BindingContext = person;
            await Navigation.PushAsync(secondPage);
            

        }
    }
}