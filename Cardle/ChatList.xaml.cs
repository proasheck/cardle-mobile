﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cardle.Models;

using Newtonsoft.Json;
namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatList : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();
        public IList<User> Users { get; private set; }
        public ChatList()
        {
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            _refreshCommand = new Command(RefrestListView);
            InitializeComponent();
            Users = new List<User>();

        }

        async void RefrestListView(object obj)
        {
           
        }

        Command _refreshCommand;
        public Command RefreshCommand
        {
            get { return _refreshCommand; }
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);
            var allUsers = await firebaseHelper.GetAllUser();
            foreach (User i in allUsers)
            {
                if(i.ID != user.ID)
                {
                    Users.Add(new User
                    {
                        ID = i.ID,
                        Username = i.Username,
                        EmailAddress = i.EmailAddress,
                        Password = i.Password,
                        Role = i.Role,
                        Rewards = i.Rewards,
                        Subscription = i.Subscription
                    });
                }
                
            }
            BindingContext = this;
        }
        
        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            User selectedItem = e.SelectedItem as User;
            User user = JsonConvert.DeserializeObject<User>(Cardle.Data.Settings.UserID);

            string person = JsonConvert.SerializeObject(user);
            //Cardle.Data.Settings.UserID = Convert.ToString(contact.ID);
            Cardle.Data.Settings.UserID = person;
            //Access Token
            string selectedReceiver = JsonConvert.SerializeObject(selectedItem);
            //Cardle.Data.Settings.UserID = Convert.ToString(contact.ID);
            Cardle.Data.Settings.ReceiverID = selectedReceiver;
            var secondPage = new Chat();
            secondPage.BindingContext = selectedReceiver;
            secondPage.BindingContext = person;
            await Navigation.PushAsync(secondPage);
        }
    }
}