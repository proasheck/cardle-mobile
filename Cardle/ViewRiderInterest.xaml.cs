﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Xamarin.Forms.Maps;
using Cardle.Models;
using Xamarin.Essentials;
namespace Cardle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewRiderInterest : ContentPage
    {
        Cardle.Data.Firebase firebaseHelper = new Cardle.Data.Firebase();

        public ViewRiderInterest()
        {
            InitializeComponent();

        }
        protected async override void OnAppearing()
        {
            Booking booking = JsonConvert.DeserializeObject<Booking>(Cardle.Data.Settings.BookingID);

            var allInterest = await firebaseHelper.GetAllInterest();

            int exist = 0;
            foreach (var i in allInterest)
            {
                if (booking.RiderID == i.UserID)
                {
                    exist = 1;
                }
            }
            if (exist == 1)
            {
                var Interest = await firebaseHelper.GetInterest(booking.RiderID);
                ethics.IsChecked = Interest.Ethics;
                geography.IsChecked = Interest.Geography;
                gaming.IsChecked = Interest.Gaming;
                history.IsChecked = Interest.History;
                literature.IsChecked = Interest.Literature;
                philosophy.IsChecked = Interest.Philosophy;
                gaming.IsChecked = Interest.Gaming;
                history.IsChecked = Interest.History;
                literature.IsChecked = Interest.Literature;
                philosophy.IsChecked = Interest.Philosophy;
                politics.IsChecked = Interest.Politics;
                sports.IsChecked = Interest.Sports;
                science.IsChecked = Interest.Science;
                technology.IsChecked = Interest.Technology;
                optout.IsChecked = Interest.OptOut;

            }
            else
            {
                await DisplayAlert("Error", "Rider has not indicated any Interest", "OK");

            }
        }
    }
}